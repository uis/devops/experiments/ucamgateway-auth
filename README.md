# Authentication client for the API Gateway

READ THE [DEVELOPER
DOCUMENTATION](https://uis.uniofcam.dev/devops/experiments/ucamgateway-auth/)
FOR MORE INFORMATION.

This library provides a selection of framework-agnostic utility functions and
classes which aim to make it easier to write clients for APIs hosted behind the
API Gateway or to implement API backends.

The library provides classes which can be used to authenticate to the University
of Cambridge API Gateway using its OAuth2 API and wrapper for
:py:mod:`requests.Session` is included which can be used to make authenticated
API requests.

Also provided are functions useful for API backends to verify authorisation
tokens from the API Gateway.

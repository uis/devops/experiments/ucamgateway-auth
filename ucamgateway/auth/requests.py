from typing import Optional

import requests
from google.auth.transport.requests import Request
from requests.auth import AuthBase

from . import Credentials


class APIGatewayAuth(AuthBase):
    """
    A :py:mod:`requests` authentication provider which can authenticate a request or session with
    the API Gateway. Usually it is more convenient to use the :py:class:`AuthenticatedSession`
    wrapper.

    :param credentials: credentials used to authenticate to the API Gateway.
    :param auth_session: if provided, a :py:class:`requests.Session` instance to use when fetching
        the authentication token from the API Gateway. If omitted, a default :py:mod:`requests`
        session is used.

    An example of use:

    .. code-block:: python

        import requests
        from ucamgateway.auth import ClientCredentials
        from ucamgateway.auth.requests import APIGatewayAuth

        # Application client id and secret.
        client_id, client_secret = "...", "..."

        credentials = ClientCredentials(
            client_id=client_id, client_secret=client_secret)
        gateway_auth = APIGatewayAuth(credentials)

        response = request.get(
            "https://api.apps.cam.ac.uk/lookup/v1/inst/UIS",
            auth=gateway_auth
        )
    """

    def __init__(
        self,
        credentials: Credentials,
        *,
        auth_session: Optional[requests.Session] = None,
    ):
        super().__init__()
        self._credentials = credentials

        # This is the transport used to actually fetch authentication tokens if necessary.
        self._credentials_request = Request(auth_session)

    def __call__(self, r: requests.PreparedRequest):
        r.headers = r.headers.copy() if r.headers is not None else {}
        self._credentials.before_request(self._credentials_request, headers=r.headers)
        return r


class AuthenticatedSession(requests.Session):
    """
    A class derived from :py:class:`requests.Session` which is pre-authenticated for making
    requests to API Gateway endpoints.

    :param credentials: credentials used to authenticate to the API Gateway.
    :param auth_session: if provided, a :py:class:`requests.Session` instance to use when fetching
        the authentication token from the API Gateway. If omitted, a default :py:mod:`requests`
        session is used.

    Other parameters are passed to the constructor of :py:class:`requests.Session`.

    An example of use:

    .. code-block:: python

        from ucamgateway.auth import ClientCredentials
        from ucamgateway.auth.requests import AuthenticatedSession

        # Application client id and secret.
        client_id, client_secret = "...", "..."

        credentials = ClientCredentials(
            client_id=client_id, client_secret=client_secret)
        session = AuthenticatedSession(credentials)
        response = session.get(
            "https://api.apps.cam.ac.uk/lookup/v1/inst/UIS")
    """

    def __init__(
        self,
        credentials: Credentials,
        *args,
        auth_session: Optional[requests.Session] = None,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.auth = APIGatewayAuth(credentials, auth_session=auth_session)

"""
Well-known URLs for API gateway endpoints.

"""
GATEWAY_TOKEN_URL = "https://api.apps.cam.ac.uk/oauth2/v1/token"

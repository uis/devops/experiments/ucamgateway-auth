import datetime
from abc import ABC, abstractmethod, abstractproperty
from typing import MutableMapping, Optional, Sequence

import structlog
from google.auth.transport import Request

LOG = structlog.get_logger()


class Credentials(ABC):
    """
    Base class for all credential implementations.

    Credentials should be agnostic as to which underlying HTTP transport library is used. If they
    need to make HTTP requests then they should make use of a
    :py:class:`google.auth.transport.Request` instance passed to them.
    """

    # How long before a token expires do we view it as expired? This skew is intended to allow a
    # bit of slop between the time we make the request and when the server verifies the expiry.
    _EXPIRY_SKEW = 30  # seconds

    @abstractproperty
    def scopes(self) -> Optional[Sequence[str]]:
        """
        The current credential's list of scopes. Requires that the credentials have been fetched at
        least once either by being used or by calling :py:meth:`refresh` explicitly.

        """

    @abstractproperty
    def token(self) -> Optional[str]:
        """
        The bearer token suitable for inserting into the Authorization header or None if we have no
        such token.

        """

    @abstractproperty
    def expiry(self) -> Optional[datetime.datetime]:
        """
        Time at which we expect the current access token is to expire. None indicates either that
        no token has yet been fetched or that the token does not expire.

        """

    @abstractmethod
    def refresh(self, request: Request) -> None:
        """
        Fetch or refresh an access token using the credentials.

        :param request: Transport object used to make any necessary HTTP requests.

        """

    @property
    def expired(self) -> bool:
        """
        True if there is an expiry time set for the token and if it has expired. False otherwise.

        """
        if not self.expiry:
            return False
        skewed_expiry = self.expiry - datetime.timedelta(seconds=self._EXPIRY_SKEW)
        return skewed_expiry <= datetime.datetime.utcnow()

    @property
    def valid(self) -> bool:
        """True if we have a token and that token has not expired."""
        return self.token is not None and not self.expired

    def apply(self, headers: MutableMapping[str, str]) -> None:
        """
        Apply the current authentication token, if any, to the mapping of headers passed.

        :param headers: mapping of headers to values which is updated with the authorization header
            required.

        """
        if not self.valid:
            return
        headers["authorization"] = f"Bearer {self.token}"

    def before_request(self, request: Request, headers: MutableMapping[str, str]) -> None:
        """
        Perform any credentials-specific processing before a request is made. Ensures the
        credential is fetched or re-fetched as necessary and, if successful, applies the token to
        the passed headers mapping via apply().

        :param request: Transport used to make any necessary HTTP requests to fetch the access
            token.
        :param headers: mapping of headers to values which is updated with the authorization header
            required.

        """
        if not self.valid:
            self.refresh(request)
        self.apply(headers)

"""
Utilities for verifying id tokens.
"""
from typing import Any, Iterable, Optional

import cachecontrol
import requests
from google.auth.exceptions import GoogleAuthError
from google.auth.transport import Request
from google.auth.transport.requests import Request as RequestsRequest
from google.oauth2 import id_token as google_id_token

__all__ = ["verify_id_token_for_api_backend", "InvalidIdTokenError"]


def _default_caching_request() -> Request:
    """
    Return a google.auth.transport.Request sub-class which caches requests for id token
    certificates.

    Uses the approach documented at
    https://google-auth.readthedocs.io/en/latest/reference/google.oauth2.id_token.html.

    """
    session = requests.session()
    cached_session = cachecontrol.CacheControl(session)
    return RequestsRequest(session=cached_session)


# Default HTTP transport used to fetch public keys for verifying tokens.
_DEFAULT_CACHING_REQUEST = _default_caching_request()

# List of Google service accounts which represent the API gateway identity.
_API_GATEWAY_SERVICE_ACCOUNTS = ["api-gateway@api-meta-2555105a.iam.gserviceaccount.com"]


class InvalidIdTokenError(ValueError):
    """
    Exception raised if token verification fails. The exception message describes the reason that
    the token failed verification.
    """


def verify_id_token_for_api_backend(
    id_token: str,
    expected_audience: str,
    *,
    request: Optional[Request] = None,
    expected_authorised_parties: Optional[Iterable[str]] = None,
) -> dict[str, Any]:
    """
    Verify an incoming authentication token from the Gateway sent to an API backend. Use this
    function when implementing API backends.

    :param id_token: id token passed to the backend by the Gateway.
    :param expected_audience: expected audience for id token. For Cloud Run-hosted backends this is
        the base URL of the application.
    :param request: HTTP transport implementation. If `None` then a default implementation based on
        the :py:mod:`requests` library is used. Note that certificates are fetched on each call to
        this function and so the transport implementation should support caching. This is the case
        for the default implementation.
    :param expected_authorised_parties: the "azp" claim in the id token must match one of the
        strings in this iterable for the token to be verified. If `None` then a list of known API
        Gateway identities is used.
    :returns: a dictionary containing the id token payload.
    """
    request = request if request is not None else _DEFAULT_CACHING_REQUEST
    expected_authorised_parties = (
        expected_authorised_parties
        if expected_authorised_parties is not None
        else _API_GATEWAY_SERVICE_ACCOUNTS
    )

    try:
        payload = google_id_token.verify_oauth2_token(id_token, request, expected_audience)
    except GoogleAuthError as e:
        raise InvalidIdTokenError(str(e)) from e

    authorized_party = payload.get("azp", "")
    if payload.get("azp", "") not in set(expected_authorised_parties):
        raise InvalidIdTokenError(f"Authorized party {authorized_party!r} not in expected set.")

    return payload

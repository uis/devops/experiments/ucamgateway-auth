import base64
import datetime
from typing import List, Optional

import structlog
from google.auth.transport import Request
from oauthlib.oauth2 import BackendApplicationClient

from ._credentials import Credentials
from ._urls import GATEWAY_TOKEN_URL

LOG = structlog.get_logger()


class ClientCredentials(Credentials):
    """
    An API Gateway client initialised with a client id, client secret and optional list of scopes
    to request from the Gateway.

    :param client_id: client id of API Gateway application. Referred to as "API key" in some
        documentation.
    :param client_secret: client secret for API Gateway application. Referred to as "API secret" in
        some documentation.
    :param scopes: list of scopes to request for token. You should always try to limit your access
        tokens to the smallest number of scopes possible.
    """

    _expiry: Optional[datetime.datetime]

    def __init__(self, client_id: str, client_secret: str, scopes: Optional[List[str]] = None):
        self._client = BackendApplicationClient(client_id=client_id, scope=scopes)
        self._client_secret = client_secret
        self._expiry = None

    @property
    def scopes(self):
        return self._client.scope

    @property
    def token(self):
        return self._client.access_token

    @property
    def expiry(self):
        return self._expiry

    def refresh(self, request: Request):
        # Prepare token fetch request.
        client_auth_basic = base64.b64encode(
            f"{self._client.client_id}:{self._client_secret}".encode("ascii")
        ).decode("ascii")
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": f"Basic {client_auth_basic}",
        }
        body = self._client.prepare_request_body()

        # Fetch token.
        response = request(url=GATEWAY_TOKEN_URL, method="POST", headers=headers, body=body)

        # Parse response.
        self._client.parse_request_body_response(response.data, scope=self._client.scope)
        if self._client.expires_in is not None:
            self._expiry = datetime.datetime.utcnow() + datetime.timedelta(
                seconds=self._client.expires_in
            )
        else:
            self._expiry = None

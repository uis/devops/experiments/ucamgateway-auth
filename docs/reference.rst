.. _reference:

Reference
=========

Client authentication
`````````````````````

Concrete credential providers
-----------------------------

The :py:mod:`ucamgateway.auth` module contains the following concrete
credentials providers.

.. autoclass:: ucamgateway.auth.ClientCredentials
    :members:

Integration with requests
-------------------------

The :py:mod:`ucamgateway.auth.requests` module defines some helper classes for
using the API Gateway via the :py:mod:`requests` module.

.. autoclass:: ucamgateway.auth.requests.AuthenticatedSession

.. autoclass:: ucamgateway.auth.requests.APIGatewayAuth

Abstract base classes
---------------------

Concrete credentials providers are derived from the following base classes from
the :py:mod:`ucamgateway.auth` module.

.. autoclass:: ucamgateway.auth.Credentials
    :members:


API Backend Utilities
`````````````````````

The :py:mod:`ucamgateway.auth` module contains the following utilities for
validating tokens from the API Gateway sent to backends.

.. autoclass:: ucamgateway.auth.id_token.verify_id_token_for_api_backend
    :members:

.. autoclass:: ucamgateway.auth.id_token.InvalidIdTokenError

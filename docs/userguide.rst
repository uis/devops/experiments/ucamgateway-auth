.. _user-guide:

User guide
==========

This page provides some examples of using the functions and classes in this
library.

Client authentication
---------------------

The following example shows how to request the name of the "UIS" institution
from Lookup. We authenticate to the API Gateway using client application
credentials and use the :py:mod:`requests` library to make HTTP requests.

.. code-block:: python

    from ucamgateway.auth import ClientCredentials
    from ucamgateway.auth.requests import AuthenticatedSession

    # Application client id and secret.
    client_id, client_secret = "...", "..."

    # Create a requests.Session authenticated with the client
    # credentials. Ask that these credentials be restricted to
    # only being able to call the Lookup API.
    credentials = ClientCredentials(
        client_id=client_id, client_secret=client_secret,
        scopes=["https://api.apps.cam.ac.uk/lookup"]
    )
    session = AuthenticatedSession(credentials)

    # Get information on the UIS institution.
    uis_inst = session.get(
        "https://api.apps.cam.ac.uk/lookup/v1/inst/UIS",
        headers={"Accept": "application/json"},
    ).json()
    print(uis_inst["result"]["institution"]["name"])


Backend verification
--------------------

The following example shows how one might validate an incoming request from the
Gateway to a backend in a Django view function.

.. code-block:: python

    from django.core.exceptions import PermissionDenied
    from ucamgateway.auth import (
        verify_id_token_for_api_backend,
        InvalidIdTokenError
    )

    def my_view_function(request):
        token_type, token = request.headers["Authorization"].split(" ")
        if token_type != "Bearer":
            raise PermissionDenied("Bearer token required")
        try:
            verify_id_token_for_api_backend(token)
        except InvalidIdTokenError as e:
            raise PermissionDenied("Invalid token") from e

        # ... remainder of view function

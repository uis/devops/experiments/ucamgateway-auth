API Gateway Authentication
==========================

.. toctree::
    :maxdepth: 2
    :hidden:

    userguide
    reference

This library provides a selection of framework-agnostic utility functions and
classes which aim to make it easier to write clients for APIs hosted behind the
API Gateway or to implement API backends.

The library provides classes which can be used to authenticate to the University
of Cambridge API Gateway using its OAuth2 API and wrapper for
:py:mod:`requests.Session` is included which can be used to make authenticated
API requests.

Also provided are functions useful for API backends to verify authorisation
tokens from the API Gateway.

The :ref:`user-guide` provides some quick usage examples and a getting started
guide.

The :ref:`reference` provides reference for the modules and classes provided.

Installation
------------

This section discusses how to install the library.

Poetry
``````

If you are using `poetry` to manage your dependencies, you can install this
library directly from the GitLab package registry.

If you've not already done so, define a "source" for the package:

.. code:: sh

   poetry source add --priority=explicit uis-devops \
     https://gitlab.developers.cam.ac.uk/api/v4/groups/5/-/packages/pypi/simple

The package can be added as a dependency by explicitly using the source:

.. code:: sh

   poetry add --source=uis-devops ucamgateway-auth

Setuptools
``````````

.. caution::

   If using setuptools, note that installation sources are not specified
   explicitly per package and so one is at risk of inadvertently installing
   similarly named packages from PyPI.

If you are using `requirements.txt` to manage dependencies, add the following
lines:

```
-i https://gitlab.developers.cam.ac.uk/api/v4/groups/5/-/packages/pypi/simple
ucamgateway-auth
```

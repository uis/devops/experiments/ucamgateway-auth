# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
project = "API Gateway Auth"
copyright = "2023, University of Cambridge Information Services"
author = "University of Cambridge Information Services"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
]

templates_path = ["_templates"]
exclude_patterns: list[str] = []

html_theme = "furo"
html_static_path = ["_static"]

add_module_names = False
autoclass_content = "both"
autodoc_member_order = "bysource"

intersphinx_mapping = {
    "google-auth": ("https://googleapis.dev/python/google-auth/latest/", None),
    "requests": ("https://requests.readthedocs.io/en/latest/", None),
}

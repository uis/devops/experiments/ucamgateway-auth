# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2023-10-02

### Added

- Documentation for `ucamgateway.auth.id_token`.

## [0.2.0] - 2023-09-20

### Added

- New module, `ucamgateway.auth.id_token` for verifying tokens passed to
  backends.

#!/usr/bin/env python3
"""
Example of using client credentials to access an API Gateway API.

You must have registered an application at https://developer.api.apps.cam.ac.uk/ and enabled the
"lookup" application.

Run this script setting the CLIENT_ID and CLIENT_SECRET environment variables:

    $ CLIENT_ID=... CLIENT_SECRET=... ./client_credentials.py
"""
import json
import os

from ucamgateway.auth import ClientCredentials
from ucamgateway.auth.requests import AuthenticatedSession

# Application client id and secret.
client_id, client_secret = os.environ["CLIENT_ID"], os.environ["CLIENT_SECRET"]

# Create a requests.Session authenticated with the client credentials. Ask that
# these credentials be restricted to only being able to call the Lookup API.
credentials = ClientCredentials(
    client_id=client_id, client_secret=client_secret, scopes=["https://api.apps.cam.ac.uk/lookup"]
)
session = AuthenticatedSession(credentials)

# Get information on the UIS institution.
uis_inst = session.get(
    "https://api.apps.cam.ac.uk/lookup/v1/inst/UIS",
    headers={"Accept": "application/json"},
).json()

print(json.dumps(uis_inst, indent=2))
